package Test;

import com.company.AList;
import com.company.IList;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AlistTest {
    IList<Integer> list = new AList<>(3);

    @Before
    public void setUp1() {

        list.add(1);
        list.add(2);
        list.add(3);


    }

    @Test
    public void clearTest() {

        list.clear();
        Assert.assertEquals(0, list.size());
    }

    @Test
    public void sizeTest() {

        Assert.assertEquals(3, list.size());
    }

    @Test
    public void getTest() {

        Assert.assertEquals(java.util.Optional.of(1).get(),list.get(0));
    }

    @Test
    public void addTest() {

        list.add(4);
        Assert.assertEquals(java.util.Optional.ofNullable(list.get(3)), java.util.Optional.of(4));
    }

    @Test
    public void add2Test() {

        list.add(0, 5);
        Assert.assertEquals(java.util.Optional.ofNullable(list.get(0)), java.util.Optional.of(5));
    }


    @Test
    public void removeTest() {

        list.remove(1);
        Assert.assertEquals(2, list.size());
    }

    @Test
    public void removeByIndexTest() {

        list.removeByIndex(0);
        Assert.assertEquals(2, list.size());
    }


    @Test
    public void containsTest() {


        Assert.assertTrue(list.contains(1));
    }
    
    @Test
    public void subListTest() {

        AList aList = new AList();
        aList.add(2);
        aList.add(3);
        IList actual =  list.subList(1,2);

        Assert.assertEquals(aList.get(0), actual.get(0));
        Assert.assertEquals(aList.get(1), actual.get(1));

    }

    @Test
    public void removeAllTest() {

        Assert.assertTrue(list.removeAll(new Integer[]{1}));

    }

    @Test
    public void retainAllTest() {

        Assert.assertTrue(list.retainAll(new Integer[]{1}));

    }

}
