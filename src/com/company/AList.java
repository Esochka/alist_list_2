package com.company;

import java.util.Arrays;

public class AList<T> implements IList<T> {

    private T[] arr;
    private int size;
    private final int capacity = 10;
    private int index;


    public AList() {
        this.arr = (T[]) new Object[capacity];
    }

    public AList(int capacity) {
        this.arr = (T[]) new Object[capacity];
    }

    public AList(T[] array) {
        this.arr = array;
    }

    @Override
    public void clear() {
        arr = (T[]) new Object[capacity];
        size=0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public T get(int index) {


        return arr[index];
    }

    @Override
    public boolean add(T t) {
        if (index == arr.length) {
            growArray();
        }
        arr[index] = t;
        index++;
        size++;
        return true;
    }

    private void growArray() {
        T[] newArray = (T[]) new Object[(int) Math.ceil((arr.length + 1) * 1.2)];
        System.arraycopy(arr, 0, newArray, 0, index);
        arr = newArray;
    }

    @Override
    public boolean add(int index, T t) {
        if (index > arr.length || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Wrong Index");
        }
        if (index > size - 1 && index < arr.length) {
            add(t);
        }
        T[] newArray = (T[]) new Object[(int) Math.ceil((arr.length + 1) * 1.2)];

        newArray[index] = t;
        System.arraycopy(arr, index, newArray, index + 1, arr.length - index);
        arr = newArray;


        return true;
    }

    @Override
    public T remove(T t) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == t) {
                removeByIndex(i);
            }
        }
        return null;
    }

    @Override
    public T removeByIndex(int index) {
        System.arraycopy(arr, index + 1, arr, index, arr.length - 1 - index);
        size--;
        return null;
    }

    @Override
    public boolean contains(T t) {
        for (int i = 0; i < size; i++) {
            if (arr[i] == t)
                return true;
        }
        return false;
    }

    @Override
    public void print() {
        System.out.println(Arrays.toString(toArray()));
    }

    @Override
    public T[] toArray() {
        T[] result = (T[]) new Object[index];
        for (int i = 0; i < index; i++) {
            result[i] = arr[i];
        }
        return result;
    }

    @Override
    public IList<T> subList(int fromIdex, int toIndex) {
        int k = 0;
        if (toIndex > index - 1 || toIndex < 0) {
            throw new ArrayIndexOutOfBoundsException("Wrong Index");
        }
        T[] result = (T[]) new Object[(toIndex - fromIdex) + 1];
        for (int i = fromIdex; i <= toIndex; i++) {
            result[k++] = arr[i];
        }
        return new AList(result);
    }

    @Override
    public boolean removeAll(T[] arr) {
        for (int i = 0; i < arr.length; i++) {
            remove(arr[i]);
        }

        return true;
    }

    @Override
    public boolean retainAll(T[] arr) {
        T[] result = (T[]) new Object[arr.length];
        int k = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (arr[i] == arr[j]) {
                    result[k++] = arr[i];
                }
            }
        }
        arr = result;
        return true;
    }
}
