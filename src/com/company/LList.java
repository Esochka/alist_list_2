package com.company;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

public class LList<T> implements IList<T> {

    private int size;

    class Node<T> {
        T t;
        Node<T> next;
        Node<T> prev;

        public Node(T t, Node<T> next, Node<T> prev) {
            this.t = t;
            this.next = next;
            this.prev = prev;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("Node{");
            sb.append("t=").append(t);
            sb.append(", next=").append(next);
            sb.append(", prev=").append(prev);
            sb.append('}');
            return sb.toString();
        }

        public Node(T t) {
            this.t = t;
            next = null;
            prev = null;
        }
    }


    private Node head;

    public LList() {
    }

    public LList(T[] array) {
        addAll(array);
    }


    @Override
    public void clear() {
        size=0;
        head = null;
    }

    @Override
    public int size() {

        return this.size;
    }

    @Override
    public T get(int index) {

        if (index < 0)
            throw new RuntimeException("Wrong Index");
        if (index == 0)
            return (T) head.t;
        Node current = null;
        if (head != null) {
            current = head.next;
            for (int i = 0; i < index - 1; i++) {
                if (current.next == null)
                    throw new RuntimeException("Wrong Index");

                current = current.next;
            }
            return (T) current.t;
        }
        throw new RuntimeException("LL Is Empty");
    }

    @Override
    public boolean add(T t) {

        Node new_node = new Node(t);

        Node last = head;

        new_node.next = null;

        if (head == null) {
            new_node.prev = null;
            head = new_node;
            size++;
            return true;
        }


        while (last.next != null)
            last = last.next;


        last.next = new_node;

        new_node.prev = last;
        size++;
        return true;
    }


    public void addAll(T[] arr) {
        for (int i = 0; i < arr.length; i++) {
            add(arr[i]);
        }
    }

    @Override
    public boolean add(int index, T t) {
        Node nptr = new Node(t, null, null);
        if (index == 0) {
            addStart(t);
            return true;
        }
        Node ptr = head;
        for (int i = 1; i <= size; i++) {
            if (i == index) {
                Node tmp = ptr.next;
                ptr.next = nptr;
                nptr.prev = ptr;
                nptr.next = tmp ;
                tmp.prev = nptr;
            }
            ptr = ptr.next;
        }
        size++;
        return true;
    }

    @Override
    public T remove(T t) {

        Node helper = new Node(0);
        helper.next = head;
        Node p = helper;

        while(p.next != null){
            if(p.next.t == t){
                Node next = p.next;
                p.next = next.next;
            }else{
                p = p.next;
            }
        }
        size--;

        return null;
    }

    @Override
    public T removeByIndex(int index) {
        Node helper = new Node(0);
        helper.next = head;
        Node p = helper;
        int count = -1;
        while(p.next != null){
            count++;
            if(count == index){
                Node next = p.next;
                p.next = next.next;
            }else{
                p = p.next;
            }
        }
        size--;
        return null;

    }

    @Override
    public boolean contains(T t) {
        Node helper = new Node(0);
        helper.next = head;
        Node p = helper;

        while(p.next != null){
            p= p.next;
            if(p.t == t){
                return true;
            }
        }

        return false;
    }

    @Override
    public void print() {
        try {
            System.out.println(Arrays.toString(toArray()));
        } catch (NullPointerException e) {
            System.out.println("LL Is Empty");
        }
    }

    @Override
    public T[] toArray() {
     /*   T[] array = (T[]) new Object[size];
        int k = 1;
        Node current = head;
        array[0] = (T) current.t;
        while (current.next != null) {
            current = current.next;
            array[k++] = (T) current.t;

        }
        Collections.reverse(Arrays.asList(array));
        return array;
*/
        T[] result = (T[]) new Object[size];
        int i = 0;
        for (Node<T> x = head; x != null; x = x.next)
            result[i++] = x.t;

        return result;
    }

    @Override
    public IList<T> subList(int fromIdex, int toIndex) {

        T [] array = toArray();
        int k=0;
        if (toIndex>size-1 || toIndex<0){
            throw new ArrayIndexOutOfBoundsException("Wrong Index");
        }
        T [] result = (T[]) new Object[(toIndex-fromIdex)+1];
        for (int i = fromIdex; i <=toIndex ; i++) {
            result[k++]= array[i];
        }
        return new LList(result);
    }

    @Override
    public boolean removeAll(T[] arr) {
        for (int i = 0; i <arr.length ; i++) {
            remove(arr[i]);
        }
        return true;
    }

    @Override
    public boolean retainAll(T[] arr) {
        T [] arra = toArray();
        int cont=0;
        for (int i = 0; i <arra.length ; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (arra[i]==arr[j]){
                    cont++;
                }
            }
        }
        T [] result = (T[]) new Object[cont];

        int k=0;
        for (int i = 0; i <arra.length ; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (arra[i]==arr[j]){
                    result[k++]=arra[i];

                }

            }
        }

        arra = result;

        clear();

        addAll(arra);

        return true;
    }

    public void addStart(T t) {

        Node n = new Node(t);
        n.next = head;
        n.prev = null;


        if (head != null) {
            head.prev = n;
        }
        head = n;

        size++;

    }

    public void addEnd(T t) {
        add(t);
    }





}
