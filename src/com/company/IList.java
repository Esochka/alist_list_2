package com.company;

public interface IList<T> {
        void clear();
        int size();
        T get(int index);
        boolean add(T t );
        boolean add(int index, T t);
        T  remove(T t );
        T  removeByIndex(int index);
        boolean contains(T t);
        void print();
        T [] toArray();
        IList<T>  subList(int fromIdex, int toIndex);
        boolean removeAll(T[] arr);
        boolean retainAll(T[] arr);
}



